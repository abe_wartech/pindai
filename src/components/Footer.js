import React, { Component } from "react";
import { Row, Col, Typography } from "antd";
import AOS from "aos";
import {
  LinkedinFilled,
  InstagramFilled,
  FacebookFilled
} from "@ant-design/icons";

const ornament = require("../images/ornament.png");

class Footer extends Component {
  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50
    });
  };
  render() {
    return (
      <Row className="section9" justify="center">
        <Col md={23}>
          <Typography className="footer1">Klik Bukan Klip</Typography>
        </Col>
        <Col md={1}>
          <img className="footer1" src={ornament} alt="1" />
        </Col>
        <Col md={24} style={{ marginTop: -20, marginBottom: 10 }}>
          <div className="logo2">
            <a href="">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="286"
                height="26"
                viewBox="0 0 286 26"
              >
                <text
                  id="Pindai_Communications"
                  data-name="Pindai Communications"
                  transform="translate(0 21)"
                  fill="#23ad46"
                  fontSize="20"
                  fontFamily="SFMono-Medium, SF Mono"
                  fontWeight="500"
                >
                  <tspan x="0" y="0">
                    Pindai Communications
                  </tspan>
                </text>
              </svg>
            </a>
            <Typography style={{ color: "white" }}>
              021.7703759 | 021.7703759
              <br />
              pindai.mk@pindai.co.id
              <br />
              Boulevard Grand Depok City Blok D1 No 12A, Depok Jawa Barat 16412
            </Typography>
            <a href="">
              <LinkedinFilled
                style={{
                  color: "white",
                  paddingTop: "2.5rem",
                  fontSize: "1.4vw"
                }}
              />
            </a>
            <a href="">
              <InstagramFilled
                style={{
                  color: "white",
                  paddingTop: "2.5rem",
                  width: "4vw",
                  fontSize: "1.4vw"
                }}
              />
            </a>
            <a href="">
              <FacebookFilled
                style={{
                  color: "white",
                  paddingTop: "2.5rem",
                  fontSize: "1.4vw"
                }}
              />
            </a>
          </div>
        </Col>
      </Row>
    );
  }
}
export default Footer;
